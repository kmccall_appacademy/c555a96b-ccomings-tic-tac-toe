class Board
  attr_reader :grid, :marks
  def initialize(grid = Board.blank_grid)
    @grid = grid
    @marks = [:X, :O]
  end

  def self.blank_grid
    Array.new(3) {Array.new(3)}
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end

  def place_mark(pos, mark)
    self[pos] = mark
  end

  def empty?(pos)
    true if self[pos].nil?
  end

  def winner
    (cols + grid + diagonals).each do |triple|
      return :X if triple == [:X,:X,:X]
      return :O if triple == [:O,:O,:O]
    end
    nil
  end

  def diagonals
    left_diag = [[0,0],[1,1],[2,2]]
    right_diag = [[0,2],[1,1],[2,0]]

    [left_diag, right_diag].map do |diag|
      diag.map {|row, col| grid[row][col]}
    end
  end

  def cols
    cols = [[],[],[]]
    grid.each do |row|
      row.each_with_index do |mark, idx|
        cols[idx] << mark
      end
    end
    cols
  end

  def over?
    grid.flatten.all? { |pos| !pos.nil? } || winner
  end
end
